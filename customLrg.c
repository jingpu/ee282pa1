/* File: customLrg.c */

#include "customLrg.h"
#include <stdlib.h>
#include <stdio.h>
#include <xmmintrin.h>

#define DIM 128
#define ROW_BSIZE 16
/* Non std stride version */
void matmul128_stride(const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  int i, j, k, kk, kk_next;
  double *a, *b, *c;
  double r;
  for (kk = 0; kk != DIM; kk = kk_next) {
     kk_next = kk + ROW_BSIZE;
     a = (double *)A; c = (double *)C;
     for (i = 0; i != DIM; ++i, a += StrideA, c += StrideC) {
        for (k = kk; k != kk_next; ++k) {
           b = (double *)B + k*StrideB;
           r = a[k];
           for (j = 0; j != DIM; j++) {
              c[j] += r*b[j];
           }
        }
     }
  }
}

void matselfadd(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
   int i,j;
  for(i = 0; i < N; i++)
    for(j = 0; j < N; j++)
      C[i*StrideC+j] += A[i*StrideA+j]+B[i*StrideB+j];
}

void matadd(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  int i,j;
  for(i = 0; i < N; i++)
    for(j = 0; j < N; j++)
      C[i*StrideC+j] = A[i*StrideA+j]+B[i*StrideB+j];
}

void matsub(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  int i,j;
  for(i = 0; i < N; i++)
    for(j = 0; j < N; j++)
      C[i*StrideC+j] = A[i*StrideA+j]-B[i*StrideB+j];
}

void matzero(int N, double *A, int StrideA) {
  int i, j;
  for(i = 0; i < N; i++)
    for(j = 0; j < N; j++)
      A[i*StrideA+j] = 0;
}

/* C = A*B */
void strassenNormal(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  
  if (N <= 128) {
    matzero(N, C, StrideC);
    matmul128_stride(A, B, C, StrideA, StrideB, StrideC);
    return;
  }
  const double *A11 = A;
  const double *A12 = A+N/2;
  const double *A21 = A+N/2*StrideA;
  const double *A22 = A21+N/2;
  const double *B11 = B;
  const double *B12 = B+N/2;
  const double *B21 = B+N/2*StrideB;
  const double *B22 = B21+N/2;
  double *C11 = C;
  double *C12 = C+N/2;
  double *C21 = C+N/2*StrideC;
  double *C22 = C21+N/2;

  double *A1122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *B1122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M1 = (double *)malloc(sizeof(double)*N/2*N/2);
  matadd(N/2, A11, A22, A1122, StrideA, StrideA, N/2);
  matadd(N/2, B11, B22, B1122, StrideB, StrideB, N/2);
  strassenNormal(N/2, A1122, B1122, M1, N/2, N/2, N/2);
  //printf("A:\n"); dispMatrix(N,A,N); 
  //printf("B:\n"); dispMatrix(N,A,N); 
  //printf("A1122:\n"); dispMatrix(N/2,A1122,N/2); 
  //printf("B1122:\n"); dispMatrix(N/2,B1122,N/2); 
  //printf("M1:\n"); dispMatrix(N/2,M1,N/2); 
  double *A2122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M2 = (double *)malloc(sizeof(double)*N/2*N/2);
  matadd(N/2, A21, A22, A2122, StrideA, StrideA, N/2);
  strassenNormal(N/2, A2122, B11, M2, N/2, StrideB, N/2);
  //printf("A2122:\n"); dispMatrix(N/2,A2122,N/2); 
  //printf("M2:\n"); dispMatrix(N/2,M2,N/2); 
  double *B12_22 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M3 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, B12, B22, B12_22, StrideB, StrideB, N/2);
  strassenNormal(N/2, A11, B12_22, M3, StrideA, N/2, N/2);
  //printf("B12_22:\n"); dispMatrix(N/2,B12_22,N/2); 
  //printf("M3:\n"); dispMatrix(N/2,M3,N/2); 
  double *B21_11 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M4 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, B21, B11, B21_11, StrideB, StrideB, N/2);
  strassenNormal(N/2, A22, B21_11, M4, StrideA, N/2, N/2);
  //printf("B21_11:\n"); dispMatrix(N/2,B21_11,N/2); 
  //printf("M4:\n"); dispMatrix(N/2,M4,N/2); 
  double *A1112 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M5 = (double *)malloc(sizeof(double)*N/2*N/2);
  matadd(N/2, A11, A12, A1112, StrideA, StrideA, N/2);
  strassenNormal(N/2, A1112, B22, M5, N/2, StrideB, N/2);
  //printf("A1112:\n"); dispMatrix(N/2,A1112,N/2); 
  //printf("M5:\n"); dispMatrix(N/2,M5,N/2); 
  double *A21_11 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *B1112 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M6 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, A21, A11, A21_11, StrideA, StrideA, N/2);
  matadd(N/2, B11, B12, B1112, StrideB, StrideB, N/2);
  strassenNormal(N/2, A21_11, B1112, M6, N/2, N/2, N/2);
  //printf("A21_11:\n"); dispMatrix(N/2,A21_11,N/2); 
  //printf("B1112:\n"); dispMatrix(N/2,B1112,N/2); 
  //printf("M6:\n"); dispMatrix(N/2,M6,N/2); 
  double *A12_22 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *B2122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M7 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, A12, A22, A12_22, StrideA, StrideA, N/2);
  matadd(N/2, B21, B22, B2122, StrideB, StrideB, N/2);
  strassenNormal(N/2, A12_22, B2122, M7, N/2, N/2, N/2);
  //printf("A12_22:\n"); dispMatrix(N/2,A12_22,N/2); 
  //printf("B2122:\n"); dispMatrix(N/2,B2122,N/2); 
  //printf("M7:\n"); dispMatrix(N/2,M7,N/2); 

  double *M14 = A1122;
  double *M7_5 = B1122;
  double *M1_2 = A2122;
  double *M36 = B12_22;
  matadd(N/2, M1, M4, M14, N/2, N/2, N/2);
  matsub(N/2, M7, M5, M7_5, N/2, N/2, N/2);
  matsub(N/2, M1, M2, M1_2, N/2, N/2, N/2);
  matadd(N/2, M3, M6, M36, N/2, N/2, N/2);
  matadd(N/2, M14, M7_5, C11, N/2, N/2, StrideC);
  matadd(N/2, M3, M5, C12, N/2, N/2, StrideC);
  matadd(N/2, M2, M4, C21, N/2, N/2, StrideC);
  matadd(N/2, M1_2, M36, C22, N/2, N/2, StrideC);
  //printf("C:\n"); dispMatrix(N,C,N); 
  
  free(A1122);
  free(B1122);
  free(A2122);
  free(B12_22);
  free(B21_11);
  free(A1112);
  free(A21_11);
  free(B1112);
  free(A12_22);
  free(B2122);
  free(M1);
  free(M2);
  free(M3);
  free(M4);
  free(M5);
  free(M6);
  free(M7);
}


/* C = C + A*B */
void strassen(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  const double *A11 = A;
  const double *A12 = A+N/2;
  const double *A21 = A+N/2*StrideA;
  const double *A22 = A21+N/2;
  const double *B11 = B;
  const double *B12 = B+N/2;
  const double *B21 = B+N/2*StrideB;
  const double *B22 = B21+N/2;
  double *C11 = C;
  double *C12 = C+N/2;
  double *C21 = C+N/2*StrideC;
  double *C22 = C21+N/2;

  double *A1122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *B1122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M1 = (double *)malloc(sizeof(double)*N/2*N/2);
  matadd(N/2, A11, A22, A1122, StrideA, StrideA, N/2);
  matadd(N/2, B11, B22, B1122, StrideB, StrideB, N/2);
  strassenNormal(N/2, A1122, B1122, M1, N/2, N/2, N/2);
#ifdef _DEBUG_strassen
  printf("A:\n"); dispMatrix(N,A,N); 
  printf("B:\n"); dispMatrix(N,A,N); 
  printf("A1122:\n"); dispMatrix(N/2,A1122,N/2); 
  printf("B1122:\n"); dispMatrix(N/2,B1122,N/2); 
  printf("M1:\n"); dispMatrix(N/2,M1,N/2); 
#endif
  double *A2122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M2 = (double *)malloc(sizeof(double)*N/2*N/2);
  matadd(N/2, A21, A22, A2122, StrideA, StrideA, N/2);
  strassenNormal(N/2, A2122, B11, M2, N/2, StrideB, N/2);
#ifdef _DEBUG_strassen
  printf("A2122:\n"); dispMatrix(N/2,A2122,N/2); 
  printf("M2:\n"); dispMatrix(N/2,M2,N/2); 
#endif
  double *B12_22 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M3 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, B12, B22, B12_22, StrideB, StrideB, N/2);
  strassenNormal(N/2, A11, B12_22, M3, StrideA, N/2, N/2);
#ifdef _DEBUG_strassen
  printf("B12_22:\n"); dispMatrix(N/2,B12_22,N/2); 
  printf("M3:\n"); dispMatrix(N/2,M3,N/2); 
#endif  
  double *B21_11 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M4 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, B21, B11, B21_11, StrideB, StrideB, N/2);
  strassenNormal(N/2, A22, B21_11, M4, StrideA, N/2, N/2);
#ifdef _DEBUG_strassen
  printf("B21_11:\n"); dispMatrix(N/2,B21_11,N/2); 
  printf("M4:\n"); dispMatrix(N/2,M4,N/2); 
#endif  
  double *A1112 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M5 = (double *)malloc(sizeof(double)*N/2*N/2);
  matadd(N/2, A11, A12, A1112, StrideA, StrideA, N/2);
  strassenNormal(N/2, A1112, B22, M5, N/2, StrideB, N/2);
#ifdef _DEBUG_strassen
  printf("A1112:\n"); dispMatrix(N/2,A1112,N/2); 
  printf("M5:\n"); dispMatrix(N/2,M5,N/2); 
#endif  
  double *A21_11 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *B1112 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M6 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, A21, A11, A21_11, StrideA, StrideA, N/2);
  matadd(N/2, B11, B12, B1112, StrideB, StrideB, N/2);
  strassenNormal(N/2, A21_11, B1112, M6, N/2, N/2, N/2);
#ifdef _DEBUG_strassen
  printf("A21_11:\n"); dispMatrix(N/2,A21_11,N/2); 
  printf("B1112:\n"); dispMatrix(N/2,B1112,N/2); 
  printf("M6:\n"); dispMatrix(N/2,M6,N/2); 
#endif  
  double *A12_22 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *B2122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M7 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, A12, A22, A12_22, StrideA, StrideA, N/2);
  matadd(N/2, B21, B22, B2122, StrideB, StrideB, N/2);
  strassenNormal(N/2, A12_22, B2122, M7, N/2, N/2, N/2);
#ifdef _DEBUG_strassen
  printf("A12_22:\n"); dispMatrix(N/2,A12_22,N/2); 
  printf("B2122:\n"); dispMatrix(N/2,B2122,N/2); 
  printf("M7:\n"); dispMatrix(N/2,M7,N/2); 
#endif  
  double *M14 = A1122;
  double *M7_5 = B1122;
  double *M1_2 = A2122;
  double *M36 = B12_22;
  matadd(N/2, M1, M4, M14, N/2, N/2, N/2);
  matsub(N/2, M7, M5, M7_5, N/2, N/2, N/2);
  matsub(N/2, M1, M2, M1_2, N/2, N/2, N/2);
  matadd(N/2, M3, M6, M36, N/2, N/2, N/2);
  matselfadd(N/2, M14, M7_5, C11, N/2, N/2, StrideC);
  matselfadd(N/2, M3, M5, C12, N/2, N/2, StrideC);
  matselfadd(N/2, M2, M4, C21, N/2, N/2, StrideC);
  matselfadd(N/2, M1_2, M36, C22, N/2, N/2, StrideC);
#ifdef _DEBUG_strassen
  printf("C:\n"); dispMatrix(N,C,N); 
#endif  
  free(A1122);
  free(B1122);
  free(A2122);
  free(B12_22);
  free(B21_11);
  free(A1112);
  free(A21_11);
  free(B1112);
  free(A12_22);
  free(B2122);
  free(M1);
  free(M2);
  free(M3);
  free(M4);
  free(M5);
  free(M6);
  free(M7);
}
