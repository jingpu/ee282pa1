/* File: custom.c */

#include "customMdm.h"
#include <stdlib.h>
#include <stdio.h>
#include <xmmintrin.h>

#define _SSE_EN

#ifndef _SSE_EN

#define DIM 64
#define ROW_BSIZE 32
void matmul64(const double *A, const double *B, double *C) {
  int i, j, k, kk, kk_next;
  double *a, *b, *c;
  double r;
  for (kk = 0; kk != DIM; kk = kk_next) {
     kk_next = kk + ROW_BSIZE;
     a = (double *)A; c = (double *)C;
     for (i = 0; i != DIM; ++i, a += DIM, c += DIM) {
        for (k = kk; k != kk_next; ++k) {
           b = (double *)B + k*DIM;
           r = a[k];
           for (j = 0; j != DIM; j++) {
              c[j] += r*b[j];
           }
        }
     }
  }
}
#undef ROW_BSIZE
#undef DIM

#define DIM 128
#define ROW_BSIZE 16
void matmul128(const double *A, const double *B, double *C) {
  int i, j, k, kk, kk_next;
  double *a, *b, *c;
  double r;
  for (kk = 0; kk != DIM; kk = kk_next) {
     kk_next = kk + ROW_BSIZE;
     a = (double *)A; c = (double *)C;
     for (i = 0; i != DIM; ++i, a += DIM, c += DIM) {
        for (k = kk; k != kk_next; ++k) {
           b = (double *)B + k*DIM;
           r = a[k];
           for (j = 0; j != DIM; j++) {
              c[j] += r*b[j];
           }
        }
     }
  }
}
#undef ROW_BSIZE
#undef DIM


/* Non uniform memory access version
   Slightly lower L1 D-Cache miss */
#define DIM 128
#define ROW_BSIZE 24
#define N_ROW_ITER 120
void matmul128_NUMA(const double *A, const double *B, double *C) {
  int i, j, k, kk, kk_next;
  double *a, *b, *c;
  double r;
  for (kk = 0; kk != N_ROW_ITER; kk = kk_next) {
     kk_next = kk + ROW_BSIZE;
     a = (double *)A; c = (double *)C;
     for (i = 0; i != DIM; ++i, a += DIM, c += DIM) {
        for (k = kk; k != kk_next; ++k) {
           b = (double *)B + k*DIM;
           r = a[k];
           for (j = 0; j != DIM; j++)
              c[j] += r*b[j];
        }
     }
  }
  a = (double *)A; c = (double *)C;
  for (i = 0; i != DIM; ++i, a += DIM, c += DIM) {
     for (k = N_ROW_ITER; k != DIM; ++k) {
        b = (double *)B + k*DIM;
        r = a[k];
        for (j = 0; j != DIM; j++)
           c[j] += r*b[j];
     }
  }
}
#undef DIM
#undef ROW_BSIZE
#undef N_ROW_ITER

#define DIM 256
#define ROW_BSIZE 8
void matmul256(const double *A, const double *B, double *C) {
  int i, j, k, kk, kk_next;
  double *a, *b, *c;
  double r;
  for (kk = 0; kk != DIM; kk = kk_next) {
     kk_next = kk + ROW_BSIZE;
     a = (double *)A; c = (double *)C;
     for (i = 0; i != DIM; ++i, a += DIM, c += DIM) {
        for (k = kk; k != kk_next; ++k) {
           b = (double *)B + k*DIM;
           r = a[k];
           for (j = 0; j != DIM; j++) {
              c[j] += r*b[j];
           }
        }
     }
  }
}

/* Do while version */
/*void matmul256(const double *A, const double *B, double *C) {
  int i, j, k, jj, kk, kk_next;
  double *a, *b, *c;
  double r;
  kk = 0;
  do {
     kk_next = kk + ROW_BSIZE;
     a = (double *)A; c = (double *)C; 
     i = 0; // i is to control loop, faster than using pointer
     do {
        for (k = kk; k != kk_next; k++) {
           b = (double *)B + k*DIM;
           r = a[k];
           for (j = 0; j != DIM; j++)
              c[j] += r*b[j];
        }
        a += DIM; c += DIM; i += 1;
     } while (i != DIM);
     kk = kk_next;
  } while (kk != DIM);
}*/
#undef ROW_BSIZE
#undef DIM


#else // if define _SSE_EN
/*
#define DIM 64
#define ROW_BSIZE 32
#define COL_BSIZE 16
#define ROW_BSIZE_DIM 2048 // ROW_BSIZE*DIM
void matmul64(const double *A, const double *B, double *C) {
  int i, j, k, jj, kk, kk_next;
  double *a, *b, *c, *cc, *aa, *bb;
  __m128d x0, x1, x2, x3, x4, x5, x6, x7;
  __m128d y0, z0, z1, z2, z3;
  b = (double *)B; kk = 0;
  do {
     kk_next = kk + ROW_BSIZE;
     a = (double *)A; c = (double *)C; 
     i = 0; // i_N is to control loop, faster than using pointer
     do {
        jj = 0; cc = c;
        do {
           k = kk; bb = b + jj; aa = a + kk;
           //_mm_prefetch((char *)(cc+DIM), _MM_HINT_T0);
           
           x0 = _mm_load_pd(cc);
           x1 = _mm_load_pd(cc+2);
           x2 = _mm_load_pd(cc+4);
           x3 = _mm_load_pd(cc+6);
           x4 = _mm_load_pd(cc+8);
           x5 = _mm_load_pd(cc+10);
           x6 = _mm_load_pd(cc+12);
           x7 = _mm_load_pd(cc+14);
           
           do {
              //_mm_prefetch((char *)(bb+DIM), _MM_HINT_T0);
              
              y0 = _mm_load1_pd(aa);
              
              z0 = _mm_load_pd(bb);
              z1 = _mm_load_pd(bb+2);
              z2 = _mm_load_pd(bb+4);
              z3 = _mm_load_pd(bb+6);
              
              z0 = _mm_mul_pd(z0, y0);
              z1 = _mm_mul_pd(z1, y0);
              z2 = _mm_mul_pd(z2, y0);
              z3 = _mm_mul_pd(z3, y0);

              x0 = _mm_add_pd(x0, z0);
              x1 = _mm_add_pd(x1, z1);
              x2 = _mm_add_pd(x2, z2);
              x3 = _mm_add_pd(x3, z3);
 
              z0 = _mm_load_pd(bb+8);
              z1 = _mm_load_pd(bb+10);
              z2 = _mm_load_pd(bb+12);
              z3 = _mm_load_pd(bb+14);
              
              z0 = _mm_mul_pd(z0, y0);
              z1 = _mm_mul_pd(z1, y0);
              z2 = _mm_mul_pd(z2, y0);
              z3 = _mm_mul_pd(z3, y0);

              x4 = _mm_add_pd(x4, z0);
              x5 = _mm_add_pd(x5, z1);
              x6 = _mm_add_pd(x6, z2);
              x7 = _mm_add_pd(x7, z3);

              bb += DIM; ++k; ++aa;
           } while (k != kk_next);
           
           _mm_store_pd(cc, x0);
           _mm_store_pd(cc+2, x1);
           _mm_store_pd(cc+4, x2);
           _mm_store_pd(cc+6, x3);
           _mm_store_pd(cc+8, x4);
           _mm_store_pd(cc+10, x5);
           _mm_store_pd(cc+12, x6);
           _mm_store_pd(cc+14, x7);

           cc += COL_BSIZE; jj += COL_BSIZE;
        } while (jj != DIM);
        a += DIM; c += DIM; ++i;
     } while (i != DIM);
     b += ROW_BSIZE_DIM; kk = kk_next;
  } while (kk != DIM);
}
#undef DIM
#undef ROW_BSIZE
#undef COL_BSIZE
#undef ROW_BSIZE_DIM


#define DIM 128
#define ROW_BSIZE 16
#define COL_BSIZE 16
#define ROW_BSIZE_DIM 2048 // ROW_BSIZE*DIM
void matmul128(const double *A, const double *B, double *C) {
  int i, j, k, jj, kk, kk_next;
  double *a, *b, *c, *cc, *aa, *bb;
  __m128d x0, x1, x2, x3, x4, x5, x6, x7;
  __m128d y0, z0, z1, z2, z3;
  b = (double *)B; kk = 0;
  do {
     kk_next = kk + ROW_BSIZE;
     a = (double *)A; c = (double *)C; 
     i = 0; // i_N is to control loop, faster than using pointer
     do {
        jj = 0; cc = c;
        do {
           k = kk; bb = b + jj; aa = a + kk;
           //_mm_prefetch((char *)(cc+DIM), _MM_HINT_T0);
           
           x0 = _mm_load_pd(cc);
           x1 = _mm_load_pd(cc+2);
           x2 = _mm_load_pd(cc+4);
           x3 = _mm_load_pd(cc+6);
           x4 = _mm_load_pd(cc+8);
           x5 = _mm_load_pd(cc+10);
           x6 = _mm_load_pd(cc+12);
           x7 = _mm_load_pd(cc+14);
           
           do {
              //_mm_prefetch((char *)(bb+DIM), _MM_HINT_T0);
              
              y0 = _mm_load1_pd(aa);
              
              z0 = _mm_load_pd(bb);
              z1 = _mm_load_pd(bb+2);
              z2 = _mm_load_pd(bb+4);
              z3 = _mm_load_pd(bb+6);
              
              z0 = _mm_mul_pd(z0, y0);
              z1 = _mm_mul_pd(z1, y0);
              z2 = _mm_mul_pd(z2, y0);
              z3 = _mm_mul_pd(z3, y0);

              x0 = _mm_add_pd(x0, z0);
              x1 = _mm_add_pd(x1, z1);
              x2 = _mm_add_pd(x2, z2);
              x3 = _mm_add_pd(x3, z3);
 
              z0 = _mm_load_pd(bb+8);
              z1 = _mm_load_pd(bb+10);
              z2 = _mm_load_pd(bb+12);
              z3 = _mm_load_pd(bb+14);
              
              z0 = _mm_mul_pd(z0, y0);
              z1 = _mm_mul_pd(z1, y0);
              z2 = _mm_mul_pd(z2, y0);
              z3 = _mm_mul_pd(z3, y0);

              x4 = _mm_add_pd(x4, z0);
              x5 = _mm_add_pd(x5, z1);
              x6 = _mm_add_pd(x6, z2);
              x7 = _mm_add_pd(x7, z3);

              bb += DIM; ++k; ++aa;
           } while (k != kk_next);
           
           _mm_store_pd(cc, x0);
           _mm_store_pd(cc+2, x1);
           _mm_store_pd(cc+4, x2);
           _mm_store_pd(cc+6, x3);
           _mm_store_pd(cc+8, x4);
           _mm_store_pd(cc+10, x5);
           _mm_store_pd(cc+12, x6);
           _mm_store_pd(cc+14, x7);

           cc += COL_BSIZE; jj += COL_BSIZE;
        } while (jj != DIM);
        a += DIM; c += DIM; ++i;
     } while (i != DIM);
     b += ROW_BSIZE_DIM; kk = kk_next;
  } while (kk != DIM);
}
#undef DIM
#undef ROW_BSIZE
#undef COL_BSIZE
#undef ROW_BSIZE_DIM

#define DIM 256
#define ROW_BSIZE 8
#define COL_BSIZE 16
#define ROW_BSIZE_DIM 2048 // ROW_BSIZE*DIM
void matmul256(const double *A, const double *B, double *C) {
  int i, j, k, jj, kk, kk_next;
  double *a, *b, *c, *cc, *aa, *bb;
  __m128d x0, x1, x2, x3, x4, x5, x6, x7;
  __m128d y0, z0, z1, z2, z3;
  b = (double *)B; kk = 0;
  do {
     kk_next = kk + ROW_BSIZE;
     a = (double *)A; c = (double *)C; 
     i = 0; // i_N is to control loop, faster than using pointer
     do {
        jj = 0; cc = c;
        do {
           k = kk; bb = b + jj; aa = a + kk;
           //_mm_prefetch((char *)(cc+DIM), _MM_HINT_T0);
           
           x0 = _mm_load_pd(cc);
           x1 = _mm_load_pd(cc+2);
           x2 = _mm_load_pd(cc+4);
           x3 = _mm_load_pd(cc+6);
           x4 = _mm_load_pd(cc+8);
           x5 = _mm_load_pd(cc+10);
           x6 = _mm_load_pd(cc+12);
           x7 = _mm_load_pd(cc+14);
           
           do {
              //_mm_prefetch((char *)(bb+DIM), _MM_HINT_T0);
              
              y0 = _mm_load1_pd(aa);
              
              z0 = _mm_load_pd(bb);
              z1 = _mm_load_pd(bb+2);
              z2 = _mm_load_pd(bb+4);
              z3 = _mm_load_pd(bb+6);
              
              z0 = _mm_mul_pd(z0, y0);
              z1 = _mm_mul_pd(z1, y0);
              z2 = _mm_mul_pd(z2, y0);
              z3 = _mm_mul_pd(z3, y0);

              x0 = _mm_add_pd(x0, z0);
              x1 = _mm_add_pd(x1, z1);
              x2 = _mm_add_pd(x2, z2);
              x3 = _mm_add_pd(x3, z3);
 
              z0 = _mm_load_pd(bb+8);
              z1 = _mm_load_pd(bb+10);
              z2 = _mm_load_pd(bb+12);
              z3 = _mm_load_pd(bb+14);
              
              z0 = _mm_mul_pd(z0, y0);
              z1 = _mm_mul_pd(z1, y0);
              z2 = _mm_mul_pd(z2, y0);
              z3 = _mm_mul_pd(z3, y0);

              x4 = _mm_add_pd(x4, z0);
              x5 = _mm_add_pd(x5, z1);
              x6 = _mm_add_pd(x6, z2);
              x7 = _mm_add_pd(x7, z3);

              bb += DIM; ++k; ++aa;
           } while (k != kk_next);
           
           _mm_store_pd(cc, x0);
           _mm_store_pd(cc+2, x1);
           _mm_store_pd(cc+4, x2);
           _mm_store_pd(cc+6, x3);
           _mm_store_pd(cc+8, x4);
           _mm_store_pd(cc+10, x5);
           _mm_store_pd(cc+12, x6);
           _mm_store_pd(cc+14, x7);

           cc += COL_BSIZE; jj += COL_BSIZE;
        } while (jj != DIM);
        a += DIM; c += DIM; ++i;
     } while (i != DIM);
     b += ROW_BSIZE_DIM; kk = kk_next;
  } while (kk != DIM);
}
#undef DIM
#undef ROW_BSIZE
#undef COL_BSIZE
#undef ROW_BSIZE_DIM
*/
#endif // end of ifdef _SSE_EN
