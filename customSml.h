#ifndef _customSml_h
#define _customSml_h

void matmul2(const double* A, const double* B, double* C);
void matmul4(const double* A, const double* B, double* C);
void matmul8(const double* A, const double* B, double* C);
void matmul16(const double* A, const double* B, double* C);
void matmul32(const double* A, const double* B, double* C);

#endif

