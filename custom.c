/* File: custom.c */

#include "custom.h"
#include <stdlib.h>
#include <stdio.h>
#include <xmmintrin.h>


/* C = C + A*B */
void matmulNonBlock(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  int i, j, k;
  for (i = 0; i < N; i++)  
    for (k = 0; k < N; k++) {
      register double r = A[i*StrideA+k];
      for (j = 0; j < N; j++)
        C[i*StrideC + j] += r*B[k*StrideB+j];
    }
}

/* C = C + A*B */
void matmulBlock(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  int i, j, k, jj, kk;

  for (kk = 0; kk < N; kk += BSIZE_L1)
    for (jj = 0; jj < N; jj += BSIZE_L1)
      for (i = 0; i < N; i ++)
        for (k = kk; k < kk + BSIZE_L1; k++) {
          register double r = A[i*StrideA+k];
          for (j = jj; j < jj + BSIZE_L1; j++)
            C[i*StrideC + j] += r*B[k*StrideB+j];
        }
}

/* C = A*B */
void matmulNonBlockNormal(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  int i, j, k;
  for (i = 0; i < N; i++)  
    for (j = 0; j < N; j++) {
      C[i*StrideC + j] = 0;
      for (k = 0; k < N; k++)
        C[i*StrideC+j] += A[i*StrideA+k]*B[k*StrideB+j];
    }
}
#define _SSE_EN
#ifndef _SSE_EN
/* Need to change to SSE */
void matmul2(const double* A, const double* B, double* C) {
   register double c0_0,c0_1,c1_0,c1_1;
   register double b0, b1;
   register double a0, a1;
   const double *A1 = A+2;
   const double *B1 = B+2;
   double *C1 = C+2;
   c0_0 = C[0]; c0_1 = C[1];
   c1_0 = C1[0]; c1_1 = C1[1];
   b0 = B[0]; b1 = B[1]; a0 = A[0];
   c0_0 += a0*b0; c0_1 += a0*b1;
   a1 = A1[0];
   c1_0 += a1*b0; c1_1 += a1*b1;

   b0 = B1[0]; b1 = B1[1]; a0 = A[1];
   c0_0 += a0*b0; c0_1 += a0*b1;
   a1 = A1[1];
   c1_0 += a1*b0; c1_1 += a1*b1;
   
   C[0] = c0_0; C[1] = c0_1;
   C1[0] = c1_0; C1[1] = c1_1;
}

void matmul4(const double* A, const double* B, double* C) {
  unsigned i, j, k;
  for (i = 0; i < 4; i++)
    for (j = 0; j < 4; j++) {
      for (k = 0; k < 4; k++)
        C[i*4+j] += A[i*4+k]*B[k*4+j];
    }
}

void matmul8(const double* A, const double* B, double* C) {
  int i, j, k;
  for (i = 0; i < 8; i++)
    for (j = 0; j < 8; j++) {
      for (k = 0; k < 8; k++)
        C[i*8+j] += A[i*8+k]*B[k*8+j];
    }
}

void matmul16(const double* A, const double* B, double* C) {
  int i, j, k;
  for (i = 0; i < 16; i++)
    for (j = 0; j < 16; j++) {
      for (k = 0; k < 16; k++)
        C[i*16+j] += A[i*16+k]*B[k*16+j];
    }
}

#else
void matmul2(const double* A, const double* B, double* C) {
// SSE2 opt
   __m128d x1, x2, x3, x4, x5, x6, x7, x8;
   x1 = _mm_load_pd(B);
   x2 = _mm_load_pd(B+2);
   x3 = _mm_load1_pd(A);
   x4 = _mm_load1_pd(A+1);
   x5 = _mm_load1_pd(A+2);
   x6 = _mm_load1_pd(A+3);
   x7 = _mm_load_pd(C);
   x8 = _mm_load_pd(C+2);

   x3 = _mm_mul_pd(x3, x1);
   x4 = _mm_mul_pd(x4, x2);
   x5 = _mm_mul_pd(x5, x1);
   x6 = _mm_mul_pd(x6, x2);


   x3 = _mm_add_pd(x3, x4);
   x5 = _mm_add_pd(x5, x6);

   x7 = _mm_add_pd(x7, x3);
   x8 = _mm_add_pd(x8, x5);

   _mm_store_pd(C, x7);
   _mm_store_pd(C+2, x8);
}

void matmul4(const double* A, const double* B, double*C ) {
// SSE2 opt
   __m128d x0, x1, x2, x3, x4, x5, x6, x7, x4b, x5b, x6b, x7b, x8, x8b;

   int i;
   for(i = 0; i < 4; i++){
     x0 = _mm_load1_pd(A);
     x1 = _mm_load1_pd(A+1);
     x2 = _mm_load1_pd(A+2);
     x3 = _mm_load1_pd(A+3);
     A+=4;  

     x4 = _mm_load_pd(B);
     x5 = _mm_load_pd(B+4);
     x6 = _mm_load_pd(B+8);
     x7 = _mm_load_pd(B+12);

     x4b = _mm_load_pd(B+2);
     x5b = _mm_load_pd(B+6);
     x6b = _mm_load_pd(B+10);
     x7b = _mm_load_pd(B+14);


     x8 = _mm_load_pd(C);
     x8b = _mm_load_pd(C+2);

     x4 = _mm_mul_pd(x0, x4);
     x5 = _mm_mul_pd(x1, x5);
     x6 = _mm_mul_pd(x2, x6);
     x7 = _mm_mul_pd(x3, x7);

     x8 = _mm_add_pd(x8, x4);
     x8 = _mm_add_pd(x8, x5);
     x8 = _mm_add_pd(x8, x6);
     x8 = _mm_add_pd(x8, x7);


     x4b = _mm_mul_pd(x0, x4b);
     x5b = _mm_mul_pd(x1, x5b);
     x6b = _mm_mul_pd(x2, x6b);
     x7b = _mm_mul_pd(x3, x7b);

     x8b = _mm_add_pd(x8b, x4b);
     x8b = _mm_add_pd(x8b, x5b);
     x8b = _mm_add_pd(x8b, x6b);
     x8b = _mm_add_pd(x8b, x7b);

     _mm_store_pd(C, x8);
     _mm_store_pd(C+2, x8b);

     C+=4;
   }
}


void matmul8(const double* A, const double* B, double* C) {
// SSE2 opt
   __m128d x0, x1, x2, x3, x4, x5, x6, x7; //reg for store matrix A
   __m128d y0, y1, y2, y3;
   __m128d z;

   int i, j;
   const double* B1;
   for(i=0; i<8; i++){
     B1 = B;
     x0 = _mm_load1_pd(A);
     x1 = _mm_load1_pd(A+1);
     x2 = _mm_load1_pd(A+2);
     x3 = _mm_load1_pd(A+3);
     x4 = _mm_load1_pd(A+4);
     x5 = _mm_load1_pd(A+5);
     x6 = _mm_load1_pd(A+6);
     x7 = _mm_load1_pd(A+7);

     for (j =0; j<4; j++){
       y0 = _mm_load_pd(B1);
       y1 = _mm_load_pd(B1+8);
       y2 = _mm_load_pd(B1+16);
       y3 = _mm_load_pd(B1+24);

       z  = _mm_load_pd(C);

       y0 = _mm_mul_pd(y0, x0);
       y1 = _mm_mul_pd(y1, x1);
       y2 = _mm_mul_pd(y2, x2);
       y3 = _mm_mul_pd(y3, x3);

       z  = _mm_add_pd(z, y0);   
       z  = _mm_add_pd(z, y1);   
       z  = _mm_add_pd(z, y2);   
       z  = _mm_add_pd(z, y3);   

       y0 = _mm_load_pd(B1+32);
       y1 = _mm_load_pd(B1+40);
       y2 = _mm_load_pd(B1+48);
       y3 = _mm_load_pd(B1+56);

       y0 = _mm_mul_pd(y0, x4);
       y1 = _mm_mul_pd(y1, x5);
       y2 = _mm_mul_pd(y2, x6);
       y3 = _mm_mul_pd(y3, x7);

       z  = _mm_add_pd(z, y0);
       z  = _mm_add_pd(z, y1);
       z  = _mm_add_pd(z, y2);
       z  = _mm_add_pd(z, y3);

       _mm_store_pd(C, z);

       B1 += 2;
       C  += 2;   
     }
     A += 8;
   }
}

/*
void matmul8for16(const double* A, const double* B, double* C) {
// SSE2 opt
   __m128d x0, x1, x2, x3, x4, x5, x6, x7; //reg for store matrix A
   __m128d y0, y1, y2, y3;
   __m128d z;

   int i, j;
   const double* B1;
   for(i=0; i<8; i++){
     B1 = B;
     x0 = _mm_load1_pd(A);
     x1 = _mm_load1_pd(A+1);
     x2 = _mm_load1_pd(A+2);
     x3 = _mm_load1_pd(A+3);
     x4 = _mm_load1_pd(A+4);
     x5 = _mm_load1_pd(A+5);
     x6 = _mm_load1_pd(A+6);
     x7 = _mm_load1_pd(A+7);

     for (j =0; j<4; j++){
       y0 = _mm_load_pd(B1);
       y1 = _mm_load_pd(B1+16);
       y2 = _mm_load_pd(B1+32);
       y3 = _mm_load_pd(B1+48);

       z  = _mm_load_pd(C);

       y0 = _mm_mul_pd(y0, x0);
       y1 = _mm_mul_pd(y1, x1);
       y2 = _mm_mul_pd(y2, x2);
       y3 = _mm_mul_pd(y3, x3);

       z  = _mm_add_pd(z, y0);   
       z  = _mm_add_pd(z, y1);   
       z  = _mm_add_pd(z, y2);   
       z  = _mm_add_pd(z, y3);   

       y0 = _mm_load_pd(B1+64);
       y1 = _mm_load_pd(B1+80);
       y2 = _mm_load_pd(B1+96);
       y3 = _mm_load_pd(B1+112);

       y0 = _mm_mul_pd(y0, x4);
       y1 = _mm_mul_pd(y1, x5);
       y2 = _mm_mul_pd(y2, x6);
       y3 = _mm_mul_pd(y3, x7);

       z  = _mm_add_pd(z, y0);
       z  = _mm_add_pd(z, y1);
       z  = _mm_add_pd(z, y2);
       z  = _mm_add_pd(z, y3);

       _mm_store_pd(C, z);

       B1 += 2;
       C  += 2;   
     }
     C += 8;
     A += 16;
   }
}
*/

void matmul16(const double* A, const double* B, double* C) {
  __m128d x0, x1, x2, x3, x4, x5, x6, x7; //reg for store matrix A
   __m128d y0, y1, y2, y3;
   __m128d z;

   int i, j, k;
   const double* A1, *B1, *C1;

   /* calculate for
   matmul8for16(A, B, C);
   matmul8for16(A, B+8, C+8);
   matmul8for16(A+128, B, C+128);
   matmul8for16(A+128, B+8, C+136);

   matmul8for16(A+8, B+128, C);
   matmul8for16(A+8, B+136, C+8);
   matmul8for16(A+136, B+128, C+128);
   matmul8for16(A+136, B+136, C+136);
   */ 

   for (k=0; k<2; k++){
     A1 = A+8*k;
     C1 = C;
     for(i=0; i<16; i++){
       x0 = _mm_load1_pd(A1);
       x1 = _mm_load1_pd(A1+1);
       x2 = _mm_load1_pd(A1+2);
       x3 = _mm_load1_pd(A1+3);
       x4 = _mm_load1_pd(A1+4);
       x5 = _mm_load1_pd(A1+5);
       x6 = _mm_load1_pd(A1+6);
       x7 = _mm_load1_pd(A1+7);

       //inner loop
       B1 = B;
       for (j =0; j<8; j++){
	 y0 = _mm_load_pd(B1);
	 y1 = _mm_load_pd(B1+16);
	 y2 = _mm_load_pd(B1+32);
	 y3 = _mm_load_pd(B1+48);

	 z  = _mm_load_pd(C1);

	 y0 = _mm_mul_pd(y0, x0);
	 y1 = _mm_mul_pd(y1, x1);
	 y2 = _mm_mul_pd(y2, x2);
	 y3 = _mm_mul_pd(y3, x3);

	 z  = _mm_add_pd(z, y0);   
	 z  = _mm_add_pd(z, y1);   
	 z  = _mm_add_pd(z, y2);   
	 z  = _mm_add_pd(z, y3);   

	 y0 = _mm_load_pd(B1+64);
	 y1 = _mm_load_pd(B1+80);
	 y2 = _mm_load_pd(B1+96);
	 y3 = _mm_load_pd(B1+112);

	 y0 = _mm_mul_pd(y0, x4);
	 y1 = _mm_mul_pd(y1, x5);
	 y2 = _mm_mul_pd(y2, x6);
	 y3 = _mm_mul_pd(y3, x7);

	 z  = _mm_add_pd(z, y0);
	 z  = _mm_add_pd(z, y1);
	 z  = _mm_add_pd(z, y2);
	 z  = _mm_add_pd(z, y3);

	 _mm_store_pd(C1, z);

	 B1 += 2;
	 C1 += 2;   
       }
       A1 += 16;
     }
     B += 128;
   }


}

void matmul32(const double* A, const double* B, double* C) {
  __m128d x0, x1, x2, x3, x4, x5, x6, x7; //reg for store matrix A
   __m128d y0, y1, y2, y3;
   __m128d z;

   int i, j, k;
   const double* A1, *B1, *C1;

   /* calculate for
   matmul8for16(A, B, C);
   matmul8for16(A, B+8, C+8);
   matmul8for16(A+128, B, C+128);
   matmul8for16(A+128, B+8, C+136);

   matmul8for16(A+8, B+128, C);
   matmul8for16(A+8, B+136, C+8);
   matmul8for16(A+136, B+128, C+128);
   matmul8for16(A+136, B+136, C+136);
   */ 

   for (k=0; k<4; k++){
     A1 = A+8*k;
     C1 = C;
     for(i=0; i<32; i++){
       x0 = _mm_load1_pd(A1);
       x1 = _mm_load1_pd(A1+1);
       x2 = _mm_load1_pd(A1+2);
       x3 = _mm_load1_pd(A1+3);
       x4 = _mm_load1_pd(A1+4);
       x5 = _mm_load1_pd(A1+5);
       x6 = _mm_load1_pd(A1+6);
       x7 = _mm_load1_pd(A1+7);

       //inner loop
       B1 = B;
       for (j =0; j<16; j++){
	 y0 = _mm_load_pd(B1);
	 y1 = _mm_load_pd(B1+32);
	 y2 = _mm_load_pd(B1+64);
	 y3 = _mm_load_pd(B1+96);

	 z  = _mm_load_pd(C1);

	 y0 = _mm_mul_pd(y0, x0);
	 y1 = _mm_mul_pd(y1, x1);
	 y2 = _mm_mul_pd(y2, x2);
	 y3 = _mm_mul_pd(y3, x3);

	 z  = _mm_add_pd(z, y0);   
	 z  = _mm_add_pd(z, y1);   
	 z  = _mm_add_pd(z, y2);   
	 z  = _mm_add_pd(z, y3);   

	 y0 = _mm_load_pd(B1+128);
	 y1 = _mm_load_pd(B1+160);
	 y2 = _mm_load_pd(B1+192);
	 y3 = _mm_load_pd(B1+224);

	 y0 = _mm_mul_pd(y0, x4);
	 y1 = _mm_mul_pd(y1, x5);
	 y2 = _mm_mul_pd(y2, x6);
	 y3 = _mm_mul_pd(y3, x7);

	 z  = _mm_add_pd(z, y0);
	 z  = _mm_add_pd(z, y1);
	 z  = _mm_add_pd(z, y2);
	 z  = _mm_add_pd(z, y3);

	 _mm_store_pd(C1, z);

	 B1 += 2;
	 C1 += 2;   
       }
       A1 += 32;
     }
     B += 256;
   }


}

#endif

void matselfadd(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  int i,j;
  for(i = 0; i < N; i++)
    for(j = 0; j < N; j++)
      C[i*StrideC+j] += A[i*StrideA+j]+B[i*StrideB+j];
}

void matadd(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  int i,j;
  for(i = 0; i < N; i++)
    for(j = 0; j < N; j++)
      C[i*StrideC+j] = A[i*StrideA+j]+B[i*StrideB+j];
}

void matsub(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  int i,j;
  for(i = 0; i < N; i++)
    for(j = 0; j < N; j++)
      C[i*StrideC+j] = A[i*StrideA+j]-B[i*StrideB+j];
}

void matzero(int N, double *A, int StrideA) {
  int i, j;
  for(i = 0; i < N; i++)
    for(j = 0; j < N; j++)
      A[i*StrideA+j] = 0;
}

/* C = A*B */
void strassenNormal(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  
  if (N <= BSIZE_L2) {
    matzero(N, C, StrideC);
    matmulBlock(N, A, B, C, StrideA, StrideB, StrideC);
    return;
  }
  const double *A11 = A;
  const double *A12 = A+N/2;
  const double *A21 = A+N/2*StrideA;
  const double *A22 = A21+N/2;
  const double *B11 = B;
  const double *B12 = B+N/2;
  const double *B21 = B+N/2*StrideB;
  const double *B22 = B21+N/2;
  double *C11 = C;
  double *C12 = C+N/2;
  double *C21 = C+N/2*StrideC;
  double *C22 = C21+N/2;

  double *A1122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *B1122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M1 = (double *)malloc(sizeof(double)*N/2*N/2);
  matadd(N/2, A11, A22, A1122, StrideA, StrideA, N/2);
  matadd(N/2, B11, B22, B1122, StrideB, StrideB, N/2);
  strassenNormal(N/2, A1122, B1122, M1, N/2, N/2, N/2);
  //printf("A:\n"); dispMatrix(N,A,N); 
  //printf("B:\n"); dispMatrix(N,A,N); 
  //printf("A1122:\n"); dispMatrix(N/2,A1122,N/2); 
  //printf("B1122:\n"); dispMatrix(N/2,B1122,N/2); 
  //printf("M1:\n"); dispMatrix(N/2,M1,N/2); 
  double *A2122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M2 = (double *)malloc(sizeof(double)*N/2*N/2);
  matadd(N/2, A21, A22, A2122, StrideA, StrideA, N/2);
  strassenNormal(N/2, A2122, B11, M2, N/2, StrideB, N/2);
  //printf("A2122:\n"); dispMatrix(N/2,A2122,N/2); 
  //printf("M2:\n"); dispMatrix(N/2,M2,N/2); 
  double *B12_22 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M3 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, B12, B22, B12_22, StrideB, StrideB, N/2);
  strassenNormal(N/2, A11, B12_22, M3, StrideA, N/2, N/2);
  //printf("B12_22:\n"); dispMatrix(N/2,B12_22,N/2); 
  //printf("M3:\n"); dispMatrix(N/2,M3,N/2); 
  double *B21_11 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M4 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, B21, B11, B21_11, StrideB, StrideB, N/2);
  strassenNormal(N/2, A22, B21_11, M4, StrideA, N/2, N/2);
  //printf("B21_11:\n"); dispMatrix(N/2,B21_11,N/2); 
  //printf("M4:\n"); dispMatrix(N/2,M4,N/2); 
  double *A1112 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M5 = (double *)malloc(sizeof(double)*N/2*N/2);
  matadd(N/2, A11, A12, A1112, StrideA, StrideA, N/2);
  strassenNormal(N/2, A1112, B22, M5, N/2, StrideB, N/2);
  //printf("A1112:\n"); dispMatrix(N/2,A1112,N/2); 
  //printf("M5:\n"); dispMatrix(N/2,M5,N/2); 
  double *A21_11 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *B1112 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M6 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, A21, A11, A21_11, StrideA, StrideA, N/2);
  matadd(N/2, B11, B12, B1112, StrideB, StrideB, N/2);
  strassenNormal(N/2, A21_11, B1112, M6, N/2, N/2, N/2);
  //printf("A21_11:\n"); dispMatrix(N/2,A21_11,N/2); 
  //printf("B1112:\n"); dispMatrix(N/2,B1112,N/2); 
  //printf("M6:\n"); dispMatrix(N/2,M6,N/2); 
  double *A12_22 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *B2122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M7 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, A12, A22, A12_22, StrideA, StrideA, N/2);
  matadd(N/2, B21, B22, B2122, StrideB, StrideB, N/2);
  strassenNormal(N/2, A12_22, B2122, M7, N/2, N/2, N/2);
  //printf("A12_22:\n"); dispMatrix(N/2,A12_22,N/2); 
  //printf("B2122:\n"); dispMatrix(N/2,B2122,N/2); 
  //printf("M7:\n"); dispMatrix(N/2,M7,N/2); 

  double *M14 = A1122;
  double *M7_5 = B1122;
  double *M1_2 = A2122;
  double *M36 = B12_22;
  matadd(N/2, M1, M4, M14, N/2, N/2, N/2);
  matsub(N/2, M7, M5, M7_5, N/2, N/2, N/2);
  matsub(N/2, M1, M2, M1_2, N/2, N/2, N/2);
  matadd(N/2, M3, M6, M36, N/2, N/2, N/2);
  matadd(N/2, M14, M7_5, C11, N/2, N/2, StrideC);
  matadd(N/2, M3, M5, C12, N/2, N/2, StrideC);
  matadd(N/2, M2, M4, C21, N/2, N/2, StrideC);
  matadd(N/2, M1_2, M36, C22, N/2, N/2, StrideC);
  //printf("C:\n"); dispMatrix(N,C,N); 
  
  free(A1122);
  free(B1122);
  free(A2122);
  free(B12_22);
  free(B21_11);
  free(A1112);
  free(A21_11);
  free(B1112);
  free(A12_22);
  free(B2122);
  free(M1);
  free(M2);
  free(M3);
  free(M4);
  free(M5);
  free(M6);
  free(M7);
}


/* C = C + A*B */
void strassen(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  const double *A11 = A;
  const double *A12 = A+N/2;
  const double *A21 = A+N/2*StrideA;
  const double *A22 = A21+N/2;
  const double *B11 = B;
  const double *B12 = B+N/2;
  const double *B21 = B+N/2*StrideB;
  const double *B22 = B21+N/2;
  double *C11 = C;
  double *C12 = C+N/2;
  double *C21 = C+N/2*StrideC;
  double *C22 = C21+N/2;

  double *A1122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *B1122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M1 = (double *)malloc(sizeof(double)*N/2*N/2);
  matadd(N/2, A11, A22, A1122, StrideA, StrideA, N/2);
  matadd(N/2, B11, B22, B1122, StrideB, StrideB, N/2);
  strassenNormal(N/2, A1122, B1122, M1, N/2, N/2, N/2);
#ifdef _DEBUG_strassen
  printf("A:\n"); dispMatrix(N,A,N); 
  printf("B:\n"); dispMatrix(N,A,N); 
  printf("A1122:\n"); dispMatrix(N/2,A1122,N/2); 
  printf("B1122:\n"); dispMatrix(N/2,B1122,N/2); 
  printf("M1:\n"); dispMatrix(N/2,M1,N/2); 
#endif
  double *A2122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M2 = (double *)malloc(sizeof(double)*N/2*N/2);
  matadd(N/2, A21, A22, A2122, StrideA, StrideA, N/2);
  strassenNormal(N/2, A2122, B11, M2, N/2, StrideB, N/2);
#ifdef _DEBUG_strassen
  printf("A2122:\n"); dispMatrix(N/2,A2122,N/2); 
  printf("M2:\n"); dispMatrix(N/2,M2,N/2); 
#endif
  double *B12_22 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M3 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, B12, B22, B12_22, StrideB, StrideB, N/2);
  strassenNormal(N/2, A11, B12_22, M3, StrideA, N/2, N/2);
#ifdef _DEBUG_strassen
  printf("B12_22:\n"); dispMatrix(N/2,B12_22,N/2); 
  printf("M3:\n"); dispMatrix(N/2,M3,N/2); 
#endif  
  double *B21_11 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M4 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, B21, B11, B21_11, StrideB, StrideB, N/2);
  strassenNormal(N/2, A22, B21_11, M4, StrideA, N/2, N/2);
#ifdef _DEBUG_strassen
  printf("B21_11:\n"); dispMatrix(N/2,B21_11,N/2); 
  printf("M4:\n"); dispMatrix(N/2,M4,N/2); 
#endif  
  double *A1112 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M5 = (double *)malloc(sizeof(double)*N/2*N/2);
  matadd(N/2, A11, A12, A1112, StrideA, StrideA, N/2);
  strassenNormal(N/2, A1112, B22, M5, N/2, StrideB, N/2);
#ifdef _DEBUG_strassen
  printf("A1112:\n"); dispMatrix(N/2,A1112,N/2); 
  printf("M5:\n"); dispMatrix(N/2,M5,N/2); 
#endif  
  double *A21_11 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *B1112 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M6 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, A21, A11, A21_11, StrideA, StrideA, N/2);
  matadd(N/2, B11, B12, B1112, StrideB, StrideB, N/2);
  strassenNormal(N/2, A21_11, B1112, M6, N/2, N/2, N/2);
#ifdef _DEBUG_strassen
  printf("A21_11:\n"); dispMatrix(N/2,A21_11,N/2); 
  printf("B1112:\n"); dispMatrix(N/2,B1112,N/2); 
  printf("M6:\n"); dispMatrix(N/2,M6,N/2); 
#endif  
  double *A12_22 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *B2122 = (double *)malloc(sizeof(double)*N/2*N/2);
  double *M7 = (double *)malloc(sizeof(double)*N/2*N/2);
  matsub(N/2, A12, A22, A12_22, StrideA, StrideA, N/2);
  matadd(N/2, B21, B22, B2122, StrideB, StrideB, N/2);
  strassenNormal(N/2, A12_22, B2122, M7, N/2, N/2, N/2);
#ifdef _DEBUG_strassen
  printf("A12_22:\n"); dispMatrix(N/2,A12_22,N/2); 
  printf("B2122:\n"); dispMatrix(N/2,B2122,N/2); 
  printf("M7:\n"); dispMatrix(N/2,M7,N/2); 
#endif  
  double *M14 = A1122;
  double *M7_5 = B1122;
  double *M1_2 = A2122;
  double *M36 = B12_22;
  matadd(N/2, M1, M4, M14, N/2, N/2, N/2);
  matsub(N/2, M7, M5, M7_5, N/2, N/2, N/2);
  matsub(N/2, M1, M2, M1_2, N/2, N/2, N/2);
  matadd(N/2, M3, M6, M36, N/2, N/2, N/2);
  matselfadd(N/2, M14, M7_5, C11, N/2, N/2, StrideC);
  matselfadd(N/2, M3, M5, C12, N/2, N/2, StrideC);
  matselfadd(N/2, M2, M4, C21, N/2, N/2, StrideC);
  matselfadd(N/2, M1_2, M36, C22, N/2, N/2, StrideC);
#ifdef _DEBUG_strassen
  printf("C:\n"); dispMatrix(N,C,N); 
#endif  
  free(A1122);
  free(B1122);
  free(A2122);
  free(B12_22);
  free(B21_11);
  free(A1112);
  free(A21_11);
  free(B1112);
  free(A12_22);
  free(B2122);
  free(M1);
  free(M2);
  free(M3);
  free(M4);
  free(M5);
  free(M6);
  free(M7);
}
