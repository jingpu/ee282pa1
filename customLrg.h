#ifndef _customLrg_h
#define _customLrg_h

void matmul128_stride(const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC);
void strassen(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC); 

#endif

