/* File: customSml.c */

#include "customSml.h"
#include <stdlib.h>
#include <stdio.h>
#include <xmmintrin.h>


#define _SSE_EN
#ifndef _SSE_EN
/* Need to change to SSE */
void matmul2(const double* A, const double* B, double* C) {
   register double c0_0,c0_1,c1_0,c1_1;
   register double b0, b1;
   register double a0, a1;
   const double *A1 = A+2;
   const double *B1 = B+2;
   double *C1 = C+2;
   c0_0 = C[0]; c0_1 = C[1];
   c1_0 = C1[0]; c1_1 = C1[1];
   b0 = B[0]; b1 = B[1]; a0 = A[0];
   c0_0 += a0*b0; c0_1 += a0*b1;
   a1 = A1[0];
   c1_0 += a1*b0; c1_1 += a1*b1;

   b0 = B1[0]; b1 = B1[1]; a0 = A[1];
   c0_0 += a0*b0; c0_1 += a0*b1;
   a1 = A1[1];
   c1_0 += a1*b0; c1_1 += a1*b1;
   
   C[0] = c0_0; C[1] = c0_1;
   C1[0] = c1_0; C1[1] = c1_1;
}

void matmul4(const double* A, const double* B, double* C) {
  unsigned i, j, k;
  for (i = 0; i < 4; i++)
    for (j = 0; j < 4; j++) {
      for (k = 0; k < 4; k++)
        C[i*4+j] += A[i*4+k]*B[k*4+j];
    }
}

void matmul8(const double* A, const double* B, double* C) {
  int i, j, k;
  for (i = 0; i < 8; i++)
    for (j = 0; j < 8; j++) {
      for (k = 0; k < 8; k++)
        C[i*8+j] += A[i*8+k]*B[k*8+j];
    }
}

void matmul16(const double* A, const double* B, double* C) {
  int i, j, k;
  for (i = 0; i < 16; i++)
    for (j = 0; j < 16; j++) {
      for (k = 0; k < 16; k++)
        C[i*16+j] += A[i*16+k]*B[k*16+j];
    }
}

#else
void matmul2(const double* A, const double* B, double* C) {
// SSE2 opt
   __m128d x1, x2, x3, x4, x5, x6, x7, x8;
   x1 = _mm_load_pd(B);
   x2 = _mm_load_pd(B+2);
   x3 = _mm_load1_pd(A);
   x4 = _mm_load1_pd(A+1);
   x5 = _mm_load1_pd(A+2);
   x6 = _mm_load1_pd(A+3);
   x7 = _mm_load_pd(C);
   x8 = _mm_load_pd(C+2);

   x3 = _mm_mul_pd(x3, x1);
   x4 = _mm_mul_pd(x4, x2);
   x5 = _mm_mul_pd(x5, x1);
   x6 = _mm_mul_pd(x6, x2);


   x3 = _mm_add_pd(x3, x4);
   x5 = _mm_add_pd(x5, x6);

   x7 = _mm_add_pd(x7, x3);
   x8 = _mm_add_pd(x8, x5);

   _mm_store_pd(C, x7);
   _mm_store_pd(C+2, x8);
}

void matmul4(const double* A, const double* B, double*C ) {
// SSE2 opt
   __m128d x0, x1, x2, x3, x4, x5, x6, x7, x4b, x5b, x6b, x7b, x8, x8b;

   int i;
   for(i = 0; i < 4; i++){
     x0 = _mm_load1_pd(A);
     x1 = _mm_load1_pd(A+1);
     x2 = _mm_load1_pd(A+2);
     x3 = _mm_load1_pd(A+3);
     A+=4;  

     x4 = _mm_load_pd(B);
     x5 = _mm_load_pd(B+4);
     x6 = _mm_load_pd(B+8);
     x7 = _mm_load_pd(B+12);

     x4b = _mm_load_pd(B+2);
     x5b = _mm_load_pd(B+6);
     x6b = _mm_load_pd(B+10);
     x7b = _mm_load_pd(B+14);


     x8 = _mm_load_pd(C);
     x8b = _mm_load_pd(C+2);

     x4 = _mm_mul_pd(x0, x4);
     x5 = _mm_mul_pd(x1, x5);
     x6 = _mm_mul_pd(x2, x6);
     x7 = _mm_mul_pd(x3, x7);

     x8 = _mm_add_pd(x8, x4);
     x8 = _mm_add_pd(x8, x5);
     x8 = _mm_add_pd(x8, x6);
     x8 = _mm_add_pd(x8, x7);


     x4b = _mm_mul_pd(x0, x4b);
     x5b = _mm_mul_pd(x1, x5b);
     x6b = _mm_mul_pd(x2, x6b);
     x7b = _mm_mul_pd(x3, x7b);

     x8b = _mm_add_pd(x8b, x4b);
     x8b = _mm_add_pd(x8b, x5b);
     x8b = _mm_add_pd(x8b, x6b);
     x8b = _mm_add_pd(x8b, x7b);

     _mm_store_pd(C, x8);
     _mm_store_pd(C+2, x8b);

     C+=4;
   }
}


void matmul8(const double* A, const double* B, double* C) {
// SSE2 opt
   __m128d x0, x1, x2, x3, x4, x5, x6, x7; //reg for store matrix A
   __m128d y0, y1, y2, y3;
   __m128d z;

   int i, j;
   const double* B1;
   for(i=0; i<8; i++){
     B1 = B;
     x0 = _mm_load1_pd(A);
     x1 = _mm_load1_pd(A+1);
     x2 = _mm_load1_pd(A+2);
     x3 = _mm_load1_pd(A+3);
     x4 = _mm_load1_pd(A+4);
     x5 = _mm_load1_pd(A+5);
     x6 = _mm_load1_pd(A+6);
     x7 = _mm_load1_pd(A+7);

     for (j =0; j<4; j++){
       y0 = _mm_load_pd(B1);
       y1 = _mm_load_pd(B1+8);
       y2 = _mm_load_pd(B1+16);
       y3 = _mm_load_pd(B1+24);

       z  = _mm_load_pd(C);

       y0 = _mm_mul_pd(y0, x0);
       y1 = _mm_mul_pd(y1, x1);
       y2 = _mm_mul_pd(y2, x2);
       y3 = _mm_mul_pd(y3, x3);

       z  = _mm_add_pd(z, y0);   
       z  = _mm_add_pd(z, y1);   
       z  = _mm_add_pd(z, y2);   
       z  = _mm_add_pd(z, y3);   

       y0 = _mm_load_pd(B1+32);
       y1 = _mm_load_pd(B1+40);
       y2 = _mm_load_pd(B1+48);
       y3 = _mm_load_pd(B1+56);

       y0 = _mm_mul_pd(y0, x4);
       y1 = _mm_mul_pd(y1, x5);
       y2 = _mm_mul_pd(y2, x6);
       y3 = _mm_mul_pd(y3, x7);

       z  = _mm_add_pd(z, y0);
       z  = _mm_add_pd(z, y1);
       z  = _mm_add_pd(z, y2);
       z  = _mm_add_pd(z, y3);

       _mm_store_pd(C, z);

       B1 += 2;
       C  += 2;   
     }
     A += 8;
   }
}

/*
void matmul8for16(const double* A, const double* B, double* C) {
// SSE2 opt
   __m128d x0, x1, x2, x3, x4, x5, x6, x7; //reg for store matrix A
   __m128d y0, y1, y2, y3;
   __m128d z;

   int i, j;
   const double* B1;
   for(i=0; i<8; i++){
     B1 = B;
     x0 = _mm_load1_pd(A);
     x1 = _mm_load1_pd(A+1);
     x2 = _mm_load1_pd(A+2);
     x3 = _mm_load1_pd(A+3);
     x4 = _mm_load1_pd(A+4);
     x5 = _mm_load1_pd(A+5);
     x6 = _mm_load1_pd(A+6);
     x7 = _mm_load1_pd(A+7);

     for (j =0; j<4; j++){
       y0 = _mm_load_pd(B1);
       y1 = _mm_load_pd(B1+16);
       y2 = _mm_load_pd(B1+32);
       y3 = _mm_load_pd(B1+48);

       z  = _mm_load_pd(C);

       y0 = _mm_mul_pd(y0, x0);
       y1 = _mm_mul_pd(y1, x1);
       y2 = _mm_mul_pd(y2, x2);
       y3 = _mm_mul_pd(y3, x3);

       z  = _mm_add_pd(z, y0);   
       z  = _mm_add_pd(z, y1);   
       z  = _mm_add_pd(z, y2);   
       z  = _mm_add_pd(z, y3);   

       y0 = _mm_load_pd(B1+64);
       y1 = _mm_load_pd(B1+80);
       y2 = _mm_load_pd(B1+96);
       y3 = _mm_load_pd(B1+112);

       y0 = _mm_mul_pd(y0, x4);
       y1 = _mm_mul_pd(y1, x5);
       y2 = _mm_mul_pd(y2, x6);
       y3 = _mm_mul_pd(y3, x7);

       z  = _mm_add_pd(z, y0);
       z  = _mm_add_pd(z, y1);
       z  = _mm_add_pd(z, y2);
       z  = _mm_add_pd(z, y3);

       _mm_store_pd(C, z);

       B1 += 2;
       C  += 2;   
     }
     C += 8;
     A += 16;
   }
}
*/

void matmul16(const double* A, const double* B, double* C) {
  __m128d x0, x1, x2, x3, x4, x5, x6, x7; //reg for store matrix A
   __m128d y0, y1, y2, y3;
   __m128d z;

   int i, j, k;
   const double* A1, *B1, *C1;

   /* calculate for
   matmul8for16(A, B, C);
   matmul8for16(A, B+8, C+8);
   matmul8for16(A+128, B, C+128);
   matmul8for16(A+128, B+8, C+136);

   matmul8for16(A+8, B+128, C);
   matmul8for16(A+8, B+136, C+8);
   matmul8for16(A+136, B+128, C+128);
   matmul8for16(A+136, B+136, C+136);
   */ 

   for (k=0; k<2; k++){
     A1 = A+8*k;
     C1 = C;
     for(i=0; i<16; i++){
       x0 = _mm_load1_pd(A1);
       x1 = _mm_load1_pd(A1+1);
       x2 = _mm_load1_pd(A1+2);
       x3 = _mm_load1_pd(A1+3);
       x4 = _mm_load1_pd(A1+4);
       x5 = _mm_load1_pd(A1+5);
       x6 = _mm_load1_pd(A1+6);
       x7 = _mm_load1_pd(A1+7);

       //inner loop
       B1 = B;
       for (j =0; j<8; j++){
	 y0 = _mm_load_pd(B1);
	 y1 = _mm_load_pd(B1+16);
	 y2 = _mm_load_pd(B1+32);
	 y3 = _mm_load_pd(B1+48);

	 z  = _mm_load_pd(C1);

	 y0 = _mm_mul_pd(y0, x0);
	 y1 = _mm_mul_pd(y1, x1);
	 y2 = _mm_mul_pd(y2, x2);
	 y3 = _mm_mul_pd(y3, x3);

	 z  = _mm_add_pd(z, y0);   
	 z  = _mm_add_pd(z, y1);   
	 z  = _mm_add_pd(z, y2);   
	 z  = _mm_add_pd(z, y3);   

	 y0 = _mm_load_pd(B1+64);
	 y1 = _mm_load_pd(B1+80);
	 y2 = _mm_load_pd(B1+96);
	 y3 = _mm_load_pd(B1+112);

	 y0 = _mm_mul_pd(y0, x4);
	 y1 = _mm_mul_pd(y1, x5);
	 y2 = _mm_mul_pd(y2, x6);
	 y3 = _mm_mul_pd(y3, x7);

	 z  = _mm_add_pd(z, y0);
	 z  = _mm_add_pd(z, y1);
	 z  = _mm_add_pd(z, y2);
	 z  = _mm_add_pd(z, y3);

	 _mm_store_pd(C1, z);

	 B1 += 2;
	 C1 += 2;   
       }
       A1 += 16;
     }
     B += 128;
   }


}

void matmul32(const double* A, const double* B, double* C) {
  __m128d x0, x1, x2, x3, x4, x5, x6, x7; //reg for store matrix A
   __m128d y0, y1, y2, y3;
   __m128d z;

   int i, j, k;
   const double* A1, *B1, *C1;

   /* calculate for
   matmul8for16(A, B, C);
   matmul8for16(A, B+8, C+8);
   matmul8for16(A+128, B, C+128);
   matmul8for16(A+128, B+8, C+136);

   matmul8for16(A+8, B+128, C);
   matmul8for16(A+8, B+136, C+8);
   matmul8for16(A+136, B+128, C+128);
   matmul8for16(A+136, B+136, C+136);
   */ 

   for (k=0; k<4; k++){
     A1 = A+8*k;
     C1 = C;
     for(i=0; i<32; i++){
       x0 = _mm_load1_pd(A1);
       x1 = _mm_load1_pd(A1+1);
       x2 = _mm_load1_pd(A1+2);
       x3 = _mm_load1_pd(A1+3);
       x4 = _mm_load1_pd(A1+4);
       x5 = _mm_load1_pd(A1+5);
       x6 = _mm_load1_pd(A1+6);
       x7 = _mm_load1_pd(A1+7);

       //inner loop
       B1 = B;
       for (j =0; j<16; j++){
	 y0 = _mm_load_pd(B1);
	 y1 = _mm_load_pd(B1+32);
	 y2 = _mm_load_pd(B1+64);
	 y3 = _mm_load_pd(B1+96);

	 z  = _mm_load_pd(C1);

	 y0 = _mm_mul_pd(y0, x0);
	 y1 = _mm_mul_pd(y1, x1);
	 y2 = _mm_mul_pd(y2, x2);
	 y3 = _mm_mul_pd(y3, x3);

	 z  = _mm_add_pd(z, y0);   
	 z  = _mm_add_pd(z, y1);   
	 z  = _mm_add_pd(z, y2);   
	 z  = _mm_add_pd(z, y3);   

	 y0 = _mm_load_pd(B1+128);
	 y1 = _mm_load_pd(B1+160);
	 y2 = _mm_load_pd(B1+192);
	 y3 = _mm_load_pd(B1+224);

	 y0 = _mm_mul_pd(y0, x4);
	 y1 = _mm_mul_pd(y1, x5);
	 y2 = _mm_mul_pd(y2, x6);
	 y3 = _mm_mul_pd(y3, x7);

	 z  = _mm_add_pd(z, y0);
	 z  = _mm_add_pd(z, y1);
	 z  = _mm_add_pd(z, y2);
	 z  = _mm_add_pd(z, y3);

	 _mm_store_pd(C1, z);

	 B1 += 2;
	 C1 += 2;   
       }
       A1 += 32;
     }
     B += 256;
   }


}

void matmul64(const double* A, const double* B, double* C) {
  __m128d x0, x1, x2, x3, x4, x5, x6, x7; //reg for store matrix A
   __m128d y0, y1, y2, y3;
   __m128d z;

   int i, j, k;
   const double* A1, *B1, *C1;



   for (k=0; k<8; k++){
     A1 = A+8*k;
     C1 = C;
     for(i=0; i<64; i++){
       x0 = _mm_load1_pd(A1);
       x1 = _mm_load1_pd(A1+1);
       x2 = _mm_load1_pd(A1+2);
       x3 = _mm_load1_pd(A1+3);
       x4 = _mm_load1_pd(A1+4);
       x5 = _mm_load1_pd(A1+5);
       x6 = _mm_load1_pd(A1+6);
       x7 = _mm_load1_pd(A1+7);

       //inner loop
       B1 = B;
       for (j =0; j<32; j++){
	 y0 = _mm_load_pd(B1);
	 y1 = _mm_load_pd(B1+64);
	 y2 = _mm_load_pd(B1+128);
	 y3 = _mm_load_pd(B1+192);

	 z  = _mm_load_pd(C1);

	 y0 = _mm_mul_pd(y0, x0);
	 y1 = _mm_mul_pd(y1, x1);
	 y2 = _mm_mul_pd(y2, x2);
	 y3 = _mm_mul_pd(y3, x3);

	 z  = _mm_add_pd(z, y0);   
	 z  = _mm_add_pd(z, y1);   
	 z  = _mm_add_pd(z, y2);   
	 z  = _mm_add_pd(z, y3);   

	 y0 = _mm_load_pd(B1+256);
	 y1 = _mm_load_pd(B1+320);
	 y2 = _mm_load_pd(B1+384);
	 y3 = _mm_load_pd(B1+448);

	 y0 = _mm_mul_pd(y0, x4);
	 y1 = _mm_mul_pd(y1, x5);
	 y2 = _mm_mul_pd(y2, x6);
	 y3 = _mm_mul_pd(y3, x7);

	 z  = _mm_add_pd(z, y0);
	 z  = _mm_add_pd(z, y1);
	 z  = _mm_add_pd(z, y2);
	 z  = _mm_add_pd(z, y3);

	 _mm_store_pd(C1, z);

	 B1 += 2;
	 C1 += 2;   
       }
       A1 += 64;
     }
     B += 512;
   }
}



void matmul128(const double* A, const double* B, double* C) {
  __m128d x0, x1, x2, x3, x4, x5, x6, x7; //reg for store matrix A
   __m128d y0, y1, y2, y3;
   __m128d z;

   int i, j, k;
   const double* A1, *B1, *C1;



   for (k=0; k<16; k++){
     A1 = A+8*k;
     C1 = C;
     for(i=0; i<128; i++){
       x0 = _mm_load1_pd(A1);
       x1 = _mm_load1_pd(A1+1);
       x2 = _mm_load1_pd(A1+2);
       x3 = _mm_load1_pd(A1+3);
       x4 = _mm_load1_pd(A1+4);
       x5 = _mm_load1_pd(A1+5);
       x6 = _mm_load1_pd(A1+6);
       x7 = _mm_load1_pd(A1+7);

       //inner loop
       B1 = B;
       for (j =0; j<64; j++){
	 y0 = _mm_load_pd(B1);
	 y1 = _mm_load_pd(B1+128);
	 y2 = _mm_load_pd(B1+256);
	 y3 = _mm_load_pd(B1+384);

	 z  = _mm_load_pd(C1);

	 y0 = _mm_mul_pd(y0, x0);
	 y1 = _mm_mul_pd(y1, x1);
	 y2 = _mm_mul_pd(y2, x2);
	 y3 = _mm_mul_pd(y3, x3);

	 z  = _mm_add_pd(z, y0);   
	 z  = _mm_add_pd(z, y1);   
	 z  = _mm_add_pd(z, y2);   
	 z  = _mm_add_pd(z, y3);   

	 y0 = _mm_load_pd(B1+512);
	 y1 = _mm_load_pd(B1+640);
	 y2 = _mm_load_pd(B1+768);
	 y3 = _mm_load_pd(B1+896);

	 y0 = _mm_mul_pd(y0, x4);
	 y1 = _mm_mul_pd(y1, x5);
	 y2 = _mm_mul_pd(y2, x6);
	 y3 = _mm_mul_pd(y3, x7);

	 z  = _mm_add_pd(z, y0);
	 z  = _mm_add_pd(z, y1);
	 z  = _mm_add_pd(z, y2);
	 z  = _mm_add_pd(z, y3);

	 _mm_store_pd(C1, z);

	 B1 += 2;
	 C1 += 2;   
       }
       A1 += 128;
     }
     B += 1024;
   }
}


void matmul256(const double* A, const double* B, double* C) {
  __m128d x0, x1, x2, x3, x4, x5, x6, x7; //reg for store matrix A
   __m128d y0, y1, y2, y3;
   __m128d z;

   int i, j, k;
   const double* A1, *B1, *C1;



   for (k=0; k<32; k++){
     A1 = A+8*k;
     C1 = C;
     for(i=0; i<256; i++){
       x0 = _mm_load1_pd(A1);
       x1 = _mm_load1_pd(A1+1);
       x2 = _mm_load1_pd(A1+2);
       x3 = _mm_load1_pd(A1+3);
       x4 = _mm_load1_pd(A1+4);
       x5 = _mm_load1_pd(A1+5);
       x6 = _mm_load1_pd(A1+6);
       x7 = _mm_load1_pd(A1+7);

       //inner loop
       B1 = B;
       for (j =0; j<128; j++){
	 y0 = _mm_load_pd(B1);
	 y1 = _mm_load_pd(B1+256);
	 y2 = _mm_load_pd(B1+512);
	 y3 = _mm_load_pd(B1+768);

	 z  = _mm_load_pd(C1);

	 y0 = _mm_mul_pd(y0, x0);
	 y1 = _mm_mul_pd(y1, x1);
	 y2 = _mm_mul_pd(y2, x2);
	 y3 = _mm_mul_pd(y3, x3);

	 z  = _mm_add_pd(z, y0);   
	 z  = _mm_add_pd(z, y1);   
	 z  = _mm_add_pd(z, y2);   
	 z  = _mm_add_pd(z, y3);   

	 y0 = _mm_load_pd(B1+1024);
	 y1 = _mm_load_pd(B1+1280);
	 y2 = _mm_load_pd(B1+1536);
	 y3 = _mm_load_pd(B1+1792);

	 y0 = _mm_mul_pd(y0, x4);
	 y1 = _mm_mul_pd(y1, x5);
	 y2 = _mm_mul_pd(y2, x6);
	 y3 = _mm_mul_pd(y3, x7);

	 z  = _mm_add_pd(z, y0);
	 z  = _mm_add_pd(z, y1);
	 z  = _mm_add_pd(z, y2);
	 z  = _mm_add_pd(z, y3);

	 _mm_store_pd(C1, z);

	 B1 += 2;
	 C1 += 2;   
       }
       A1 += 256;
     }
     B += 2048;
   }
}


#define Dim 512
void matmul512(const double* A, const double* B, double* C) {
  __m128d x0, x1, x2, x3, x4, x5, x6; //reg for store matrix A
  __m128d y0, y1, y2, y3, y4, y5 ,y6;
  __m128d z;

   int i, j, k, l;
   const double* A1, *B1,*B2, *C1, *C2;

   for (k=0; k<Dim/7; k++){
       A1 = A+7*k;
       B2 = B;
       C1 = C;
       for(i=0; i<Dim; i++){
	 x0 = _mm_load1_pd(A1);
	 x1 = _mm_load1_pd(A1+1);
	 x2 = _mm_load1_pd(A1+2);
	 x3 = _mm_load1_pd(A1+3);
	 x4 = _mm_load1_pd(A1+4);
	 x5 = _mm_load1_pd(A1+5);
	 x6 = _mm_load1_pd(A1+6);

	 //inner loop
	 B1 = B2;
	 for (j =0; j<Dim/2; j++){
	   y0 = _mm_load_pd(B1);
	   y1 = _mm_load_pd(B1+Dim);
	   y2 = _mm_load_pd(B1+Dim*2);
	   y3 = _mm_load_pd(B1+Dim*3);
	   y4 = _mm_load_pd(B1+Dim*4);
	   y5 = _mm_load_pd(B1+Dim*5);
	   y6 = _mm_load_pd(B1+Dim*6);

	   z  = _mm_load_pd(C1);

	   y0 = _mm_mul_pd(y0, x0);
	   y1 = _mm_mul_pd(y1, x1);
	   y2 = _mm_mul_pd(y2, x2);
	   y3 = _mm_mul_pd(y3, x3);
	   y4 = _mm_mul_pd(y4, x4);
	   y5 = _mm_mul_pd(y5, x5);
	   y6 = _mm_mul_pd(y6, x6);

	   z  = _mm_add_pd(z, y0);
	   z  = _mm_add_pd(z, y1);
	   z  = _mm_add_pd(z, y2);
	   z  = _mm_add_pd(z, y3);
	   z  = _mm_add_pd(z, y4);
	   z  = _mm_add_pd(z, y5);
	   z  = _mm_add_pd(z, y6);

	   _mm_store_pd(C1, z);

	   B1 += 2;
	   C1 += 2;   
	 }
	 A1 += Dim;
       }
     
     B += Dim*7;
   }

       A1 = A+7*k;
       B2 = B;
       C1 = C;
       for(i=0; i<Dim; i++){
	 x0 = _mm_load1_pd(A1);
	 //inner loop
	 B1 = B2;
	 for (j =0; j<Dim/2; j++){
	   y0 = _mm_load_pd(B1);

	   z  = _mm_load_pd(C1);

	   y0 = _mm_mul_pd(y0, x0);

	   z  = _mm_add_pd(z, y0);

	   _mm_store_pd(C1, z);

	   B1 += 2;
	   C1 += 2;   
	 }
	 A1 += Dim;
       }

}
#undef Dim



#define Dim 2048
#define Seg 4
void matmul2048(const double* A, const double* B, double* C) {
  __m128d x0, x1, x2, x3, x4, x5, x6; //reg for store matrix A
  __m128d y0, y1, y2, y3, y4, y5 ,y6;
  __m128d z;

   int i, j, k, l;
   const double* A1, *B1,*B2, *C1, *C2;

   for (k=0; k<Dim/7; k++){
     for(l=0;l<Seg;l++){
       A1 = A+7*k;
       B2 = B+l*(Dim/Seg);
       C1 = C+l*(Dim/Seg);
       for(i=0; i<Dim; i++){
	 x0 = _mm_load1_pd(A1);
	 x1 = _mm_load1_pd(A1+1);
	 x2 = _mm_load1_pd(A1+2);
	 x3 = _mm_load1_pd(A1+3);
	 x4 = _mm_load1_pd(A1+4);
	 x5 = _mm_load1_pd(A1+5);
	 x6 = _mm_load1_pd(A1+6);

	 //inner loop
	 B1 = B2;
	 for (j =0; j<Dim/Seg/2; j++){
	   y0 = _mm_load_pd(B1);
	   y1 = _mm_load_pd(B1+Dim);
	   y2 = _mm_load_pd(B1+Dim*2);
	   y3 = _mm_load_pd(B1+Dim*3);
	   y4 = _mm_load_pd(B1+Dim*4);
	   y5 = _mm_load_pd(B1+Dim*5);
	   y6 = _mm_load_pd(B1+Dim*6);

	   z  = _mm_load_pd(C1);

	   y0 = _mm_mul_pd(y0, x0);
	   y1 = _mm_mul_pd(y1, x1);
	   y2 = _mm_mul_pd(y2, x2);
	   y3 = _mm_mul_pd(y3, x3);
	   y4 = _mm_mul_pd(y4, x4);
	   y5 = _mm_mul_pd(y5, x5);
	   y6 = _mm_mul_pd(y6, x6);

	   z  = _mm_add_pd(z, y0);
	   z  = _mm_add_pd(z, y1);
	   z  = _mm_add_pd(z, y2);
	   z  = _mm_add_pd(z, y3);
	   z  = _mm_add_pd(z, y4);
	   z  = _mm_add_pd(z, y5);
	   z  = _mm_add_pd(z, y6);

	   _mm_store_pd(C1, z);

	   B1 += 2;
	   C1 += 2;   
	 }
	 C1 += Dim*(Seg-1)/Seg;
	 A1 += Dim;
       }
     }
     B += Dim*7;
   }
     for(l=0;l<Seg;l++){
       A1 = A+7*k;
       B2 = B+l*(Dim/Seg);
       C1 = C+l*(Dim/Seg);
       for(i=0; i<Dim; i++){
	 x0 = _mm_load1_pd(A1);
	 x1 = _mm_load1_pd(A1+1);
	 x2 = _mm_load1_pd(A1+2);
	 x3 = _mm_load1_pd(A1+3);

	 //inner loop
	 B1 = B2;
	 for (j =0; j<Dim/Seg/2; j++){
	   y0 = _mm_load_pd(B1);
	   y1 = _mm_load_pd(B1+Dim);
	   y2 = _mm_load_pd(B1+Dim*2);
	   y3 = _mm_load_pd(B1+Dim*3);

	   z  = _mm_load_pd(C1);

	   y0 = _mm_mul_pd(y0, x0);
	   y1 = _mm_mul_pd(y1, x1);
	   y2 = _mm_mul_pd(y2, x2);
	   y3 = _mm_mul_pd(y3, x3);

	   z  = _mm_add_pd(z, y0);
	   z  = _mm_add_pd(z, y1);
	   z  = _mm_add_pd(z, y2);
	   z  = _mm_add_pd(z, y3);

	   _mm_store_pd(C1, z);

	   B1 += 2;
	   C1 += 2;   
	 }
	 C1 += Dim*(Seg-1)/Seg;
	 A1 += Dim;
       }
     }
}
#undef Dim
#undef Seg

#endif
