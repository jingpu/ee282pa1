#ifndef _customMdm_h
#define _customMdm_h

void matmul64(const double *A, const double *B, double *C); 
void matmul128(const double *A, const double *B, double *C); 
void matmul128_NUMA(const double *A, const double *B, double *C); 
void matmul256(const double *A, const double *B, double *C); 

#endif

