#ifndef _custom_h
#define _custom_h

#define BSIZE_L1 32
#define BSIZE_L2 128



void mul_mfmf_mf(const int M, const int K, const int N, const double *const A, const double *const B, double *const C, const int Astride, const int Bstride, const int Cstride);

void matmul2(const double* A, const double* B, double* C);
 
void matmul4(const double* A, const double* B, double* C);

void matmul8(const double* A, const double* B, double* C);

void matmul16(const double* A, const double* B, double* C);

void matmul32(const double* A, const double* B, double* C);

void matmulNonBlock(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC);

void matmulBlock(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC);


#endif

