// File: custonTest.c

//#include "customTest.h"

/* C = C + A*B */
void matmulNonBlock(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  int i, j, k;
  for (i = 0; i < N; i++)  
    for (k = 0; k < N; k++) {
      register double r = A[i*StrideA+k];
      for (j = 0; j < N; j++)
        C[i*StrideC + j] += r*B[k*StrideB+j];
    }
}

/* C = C + A*B */
void matmulBlock(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  int i, j, k, jj, kk;

  for (kk = 0; kk < N; kk += BSIZE_L1)
    for (jj = 0; jj < N; jj += BSIZE_L1)
      for (i = 0; i < N; i ++)
        for (k = kk; k < kk + BSIZE_L1; k++) {
          register double r = A[i*StrideA+k];
          for (j = jj; j < jj + BSIZE_L1; j++)
            C[i*StrideC + j] += r*B[k*StrideB+j];
        }
}

/* C = A*B */
void matmulNonBlockNormal(int N, const double *A, const double *B, double *C, int StrideA, int StrideB, int StrideC) {
  int i, j, k;
  for (i = 0; i < N; i++)  
    for (j = 0; j < N; j++) {
      C[i*StrideC + j] = 0;
      for (k = 0; k < N; k++)
        C[i*StrideC+j] += A[i*StrideA+k]*B[k*StrideB+j];
    }
}

