/* File: matmul.c */
// This is the matrix multiply kernel you are to replace.
// Note that you are to implement C = C + AB, not C = AB!
#include "customSml.h"
#include "customMdm.h"
#include "customLrg.h"
#include <stdlib.h>
#include <cblas.h>

void matmul(int N, const double* A, const double* B, double* C) {
  
  switch (N) {
    case 2: matmul2(A, B, C); break;
    case 4: matmul4(A, B, C); break;
    case 8: matmul8(A, B, C); break;
    case 16: matmul16(A, B, C); break;
    case 32: matmul32(A, B, C); break;
    case 64: matmul64(A, B, C); break;
    case 128: matmul128(A, B, C); break;
    case 256: matmul256(A, B, C); break;
    case 512: matmul512(A, B, C); break;
    case 1024: strassen(N,  A, B, C, N, N, N); break;
    case 2048:  matmul2048(A, B, C); break;
    default: printf("Dim = %d not implemented!", N);
  }

// BLAS 3
/*
  cblas_dgemm(CblasRowMajor,
              CblasNoTrans,
              CblasNoTrans,
              N, N, N,
              1.0, A, N, B, N, 1.0, C, N);
*/

// Original
/*
  for (i = 0; i < N; i++)
    for (j = 0; j < N; j++)
      for (k = 0; k < N; k++)  
          C[i*N+j] += A[i*N+k]*B[k*N+j];
*/

/*if (N >= 512) {
    for (kk = 0; kk < N; kk += 8)
      for (jj = 0; jj < N; jj += 512)
        for (i_N = 0; i_N < N_N; i_N += N)
          for (k = kk; k < kk + 8; k++) {
            register double r = A[i_N+k];
            for (j = jj; j < jj + 512; j++)
              C[i_N + j] += r*B[k*N+j];
}}*/
   /*for (i = 0; i < N; i++)
    for (j = 0; j < N; j++)
      for (k = 0; k < N; k++)
        C[i*N+j] += A[i*N+k] * B[k*N+j];*/
  /*if (N == 2) matmul2(A, B, C, N, N, N);
  else if (N <= BSIZE_L1) matmulNonBlock(N, A, B, C, N, N, N);
  else if (N <= BSIZE_L2) matmulBlock(N, A, B, C, N, N, N);
  else strassen(N, A, B, C, N, N, N);*/
  
  /* For testing, currently have round-off error */
  //strassen(N, A, B, C, N, N, N);
  
  //matmulNonBlock(N, A, B, C, N, N, N);
 
 /*if (N == 2) {
   matmul2(A,B,C);
 } else if (N <= BSIZE_L1) {
    for (i_N = 0; i_N < N_N; i_N += N) {
      for (k = 0; k < N; k++) {
        register double r = A[i_N+k];
        for (j = 0; j < N; j++)
          C[i_N + j] += r*B[k*N+j];
      }
    }
  } else if (N <= BSIZE_L2) { 
    for (kk = 0; kk < N; kk += BSIZE_L1)
      for (jj = 0; jj < N; jj += BSIZE_L1)
        for (i_N = 0; i_N < N_N; i_N += N)
          for (k = kk; k < kk + BSIZE_L1; k++) {
            register double r = A[i_N+k];
            for (j = jj; j < jj + BSIZE_L1; j++)
              C[i_N + j] += r*B[k*N+j];
          }
  } else {
    for (ii = 0; ii < N; ii += BSIZE_L2)
      for (kk = 0; kk < N; kk += BSIZE_L1)
        for (jj = 0; jj < N; jj += BSIZE_L1)
          for (i = ii; i < ii + BSIZE_L2; i++)
            for (k = kk; k < kk + BSIZE_L1; k++)
              for (j = jj; j < jj + BSIZE_L1; j++)
                C[i*N + j] += A[i*N+k]*B[k*N+j];
  }*/
}

