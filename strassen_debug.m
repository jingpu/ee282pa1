% strassen debug

N = 8;
A = importdata('Dim_8.initA');
B = importdata('Dim_8.initB');
C = importdata('Dim_8.initC');
A11 = A(1:N/2, 1:N/2);
A12 = A(1:N/2, N/2+1:N);
A21 = A(N/2+1:N, 1:N/2);
A22 = A(N/2+1:N, N/2+1:N);
B11 = B(1:N/2, 1:N/2);
B12 = B(1:N/2, N/2+1:N);
B21 = B(N/2+1:N, 1:N/2);
B22 = B(N/2+1:N, N/2+1:N);
C11 = C(1:N/2, 1:N/2);
C12 = C(1:N/2, N/2+1:N);
C21 = C(N/2+1:N, 1:N/2);
C22 = C(N/2+1:N, N/2+1:N);
M1 = (A11+A22)*(B11+B22);
M2 = (A21+A22)*B11;
M3 = A11*(B12-B22);
M4 = A22*(B21-B11);
M5 = (A11+A12)*B22;
M6 = (A21-A11)*(B11+B12);
M7 = (A12-A22)*(B21+B22);
C11 = C11+M1+M4-M5+M7;
C12 = C12+M3+M5;
C21 = C21+M2+M4;
C22 = C22+M1-M2+M3+M6;
C_final = [C11, C12; C21, C22];
C_goal = C+A*B;
error = C_final-C_goal

